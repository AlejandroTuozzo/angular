import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    selected: boolean;
    id = uuid();
    public servicios: string[];
    constructor(public nombre: string, public imagenURL: string, public votes: number = 0) {
        this.servicios = ['pileta', 'desayuno'];
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(selected: boolean) {
        this.selected = selected;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
    resetVote() {
        this.votes = 0;
    }
}
